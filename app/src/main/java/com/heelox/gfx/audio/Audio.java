package com.heelox.gfx.audio;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;

import java.io.IOException;

/**
 * Created by Dawid on 28/05/2015.
 */
public class Audio implements IAudio {

    public static String TAG = "GameFramework/Audio";

    /**
     * Asset manager is used to access the files stored in Assets
     */
    private AssetManager assets;
    /**
     * Will have all the game effects
     */
    private SoundPool soundPool;


    /**
     * This class is an extension to the activity (hence the activity needs to be passed to make any sense of what is going on)
     * @param activity the activity of the game
     */
    public Audio(Activity activity) {
        Log.v(TAG, "Audio Manager has been created");
        /**
         * This sets the volume in the Music. This way when you want to turn the music down
         * it will turn the music down not the ringtone.
         */
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        Log.v(TAG, "Set the volume control stream to STREAM_MUSIC");
        this.assets = activity.getAssets();
        Log.v(TAG, "Got the asset manager.");
        this.soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
        Log.v(TAG, "Created the soundpool");
    }

    /**
     * @see IAudio
     */
    public IMusic createMusic(String filename) throws IOException {
        Log.v(TAG, "createMusic('" + filename + "')");
        try {
            AssetFileDescriptor descriptor = assets.openFd(filename);
            return new Music(descriptor);
        } catch(IOException e) {
            Log.e(TAG, "Couldn't load music from: '" + filename + "'");
            throw new IOException(e);
        }
    }

    /**
     * @see IAudio
     */
    public ISound createSound(String filename) throws IOException {
        Log.v(TAG, "createSound('" + filename + "')");
        try {
            AssetFileDescriptor descriptor = assets.openFd(filename);
            int soundId = soundPool.load(descriptor, 0);
            return new Sound(this.soundPool, soundId);
        } catch (IOException e) {
            Log.e(TAG, "");
            throw new IOException(e);
        }
    }

}
