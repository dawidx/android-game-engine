package com.heelox.gfx.audio;

/**
 * The IMusic interface allows you to manipulate music in the game.
 * Created by 310167754 on 28/05/2015.
 */
public interface IMusic {

    /**
     * Starts playing the music
     */
    public void play();

    /**
     * Stops the music and returns to the beginning
     */
    public void stop();

    /**
     * Stops the music playing
     */
    public void pause();

    /**
     * Sets music to repeat itself
     * @param looping true if you want the music to repeat
     */
    public void setLooping(boolean looping);

    /**
     * Sets the volume of the music
     * @param volume should be between 0 and 1
     */
    public void setVolume(float volume);

    /**
     * Checks if the music is currently playing
     * @return
     */
    public boolean isPlaying();

    /**
     * Checks if the music is not playing and the timer is set to 0
     * @return
     */
    public boolean isStopped();

    /**
     * Disposes of the music object
     */
    public void dispose();
}
