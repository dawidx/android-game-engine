package com.heelox.gfx.audio;

import java.io.IOException;

/**
 * This is an audio interface. It will be used to ensure that if I improve the library
 * the game doesn't crash.
 */
public interface IAudio {

    /**
     * This method takes path to the audio file and converts it into a IMusic Object
     * @param file path to the MP3 file in the assets
     * @return IMusic object
     */
    public IMusic createMusic(String file) throws IOException;

    /**
     * This method takes path to the audio file and returns ISound Object.
     * @param file path to the MP3 file in the assets
     * @return ISound object
     */
    public ISound createSound(String file) throws IOException;
}
