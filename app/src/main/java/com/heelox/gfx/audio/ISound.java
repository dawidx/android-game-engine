package com.heelox.gfx.audio;

/**
 * Unlike the IMusic interface, ISound is mainly used for game effects (sound of bullets etc)
 * It is not supposed to be used for longer music
 * Created by 310167754 on 28/05/2015.
 */
public interface ISound {

    /**
     * Plays the game effects
     * @param volume of the
     */
    public void play(float volume);

    /**
     * Disposes the object
     */
    public void dispose();

}
