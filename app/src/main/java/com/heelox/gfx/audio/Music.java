package com.heelox.gfx.audio;

import android.content.res.AssetFileDescriptor;

/**
 * Created by 310167754 on 28/05/2015.
 */
public class Music implements IMusic {

    public Music(AssetFileDescriptor descriptor) {

    }

    @Override
    public void play() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void setLooping(boolean looping) {

    }

    @Override
    public void setVolume(float volume) {

    }

    @Override
    public boolean isPlaying() {
        return false;
    }

    @Override
    public boolean isStopped() {
        return false;
    }

    @Override
    public void dispose() {

    }
}
