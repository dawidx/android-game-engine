package com.heelox.gfx.game;

/**
 * Created by 310167754 on 10/06/2015.
 */
public class GameException extends Exception {

    public GameException(String message) {
        super(message);
    }
}
