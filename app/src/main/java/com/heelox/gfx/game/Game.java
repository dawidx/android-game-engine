package com.heelox.gfx.game;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.heelox.gfx.audio.Audio;
import com.heelox.gfx.audio.IAudio;
import com.heelox.gfx.touch.Input;
import com.heelox.gfx.types.Tuple;

/**
 * @licence THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * Created by Heelox Team on 10/06/2015.
 * The activity used for creating games. There are several attributes that allow faster development inc. full screen and orientation settings.
 */
public class Game extends Activity {

    private static final String TAG = "GFx/Game";

    protected Tuple<Integer, Integer> frameBuffer;
    //Game Elements

    /**
     * Allows you to control audio sounds in the game.
     */
    protected IAudio audio = new Audio(this);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(TAG, "Executed onCreate()");
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Log.v(TAG, "Successfully requested window without the title bar.");
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Log.v(TAG, "Toggled to fullscreen");
    }

    /**
     * The method should be called when all of the game settings are set.
     */
    protected void start() {

    }

    /**
     * This scale factor is then later used to scale objects
     * @param screenSize
     * @param frameBuffer
     * @return
     */
    private Tuple<Float, Float> getScreenScale(Tuple<Integer, Integer> screenSize, Tuple<Integer, Integer> frameBuffer)
    {
        float scaleX = frameBuffer.x / screenSize.x;
        float scaleY = frameBuffer.y / screenSize.y;
        Log.d(TAG, "The calculated scales are: x = " + Float.toString(scaleX) + " and y = " + Float.toString(scaleY));
        return new Tuple<>(scaleX, scaleY);
    }

    /**
     * The method returns the screen size (convention LWH) but there is no length.
     * @return Tuple WIDTH and HEIGHT
     */
    private Tuple<Integer, Integer> getScreenSize() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels;
        int screenHeight = displaymetrics.heightPixels;
        Log.d(TAG, "The width of the screen is " + Integer.toString(screenWidth) + " and height " + Integer.toString(screenWidth));
        return new Tuple<>(screenWidth, screenHeight);
    }

    /**
     * Can be used to override the default settings for the frame buffer.
     * @param size
     */
    protected void setFrameBuffer(Tuple<Integer, Integer> size) {
        if(size.x > 800) {
            Log.w(TAG, "The resolution of your frame width is really big. ");
        }
        if(size.y > 400){
            Log.w(TAG, "The resolution of your frame width is really big. ");
        }
        this.frameBuffer = size;
    }


    protected Tuple<Integer, Integer> getFrameBufferHeight() {
        return this.frameBuffer;
    }

}
