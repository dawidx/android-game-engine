package com.heelox.gfx.graphics;

import android.graphics.Rect;
import android.util.Log;
import android.view.SurfaceView;

import com.heelox.gfx.game.Game;

/**
 * The class enables quick view rendering capabilities. This is kind of an extension to the Game class
 * So rather than doing everything on the UI thread we can manage the state of the application
 * from other levels
 * Created by Heelox Team on 10/06/2015.
 */
public class FastViewRender extends SurfaceView implements Runnable {

    public static final String TAG = "GFx/FastViewRender";
    /**
     * The state of this variable indicates whether the game is currently running.
     */
    private volatile boolean running = false;


    private Game game;
    private Thread renderThread = null;

    public FastViewRender(Game context) {
        super(context);
        this.game = context;
    }

    public void resume() {
        Log.v(TAG, "resume()");
        this.running = true;
        Log.v(TAG, "Running = true");
        this.renderThread = new Thread(this);
        this.renderThread.start();
    }

    public void pause() {
        Log.v(TAG, "pause()");
        this.running = false;
        Log.v(TAG, "Running = false");
        while(true) {
            try {
                this.renderThread.join();
                break;
            } catch (InterruptedException e) {
                Log.e(TAG, "InterruptedException has occurred when the application was paused.");
            }
        }
    }

    @Override
    public void run() {
        Log.v(TAG, "run()");
        Rect rec = new Rect();
    }
}
