package com.heelox.gfx.touch;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.heelox.gfx.types.Tuple;

/**
 * @licence THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * Created by Heelox Team on 10/06/2015.
 */
public class Input {

    public static final String TAG = "GFx/Input";
    private Activity activity;

    public Input(Activity activity, View view, Tuple<Integer, Integer> scales) {
        this.activity = activity;
    }

}
